const User = require('../models/user');

module.exports.getProfile = (userId) => {
	return User.findById(userId).then((user,error) =>{
		if(error){
			console.log(error)
			return false;
		}
		user.password = "";
		user.save().then(updatedUser =>{
			return updatedUser
		})
		return user
	})
}

