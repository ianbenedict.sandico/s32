const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');

router.post('/:id', (req,res) =>{
	userController.getProfile(req.params.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

module.exports = router;